import com.family.dao.FamilyDao;
import com.family.model.Family;
import com.family.model.Human;
import com.family.model.Pet;
import com.family.service.FamilyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import com.family.dao.FamilyDao;
import com.family.model.Family;
import com.family.model.Human;
import com.family.model.Pet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class FamilyServiceTest {
    @Mock
    private FamilyDao familyDao;

    private FamilyService familyService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        familyService = new FamilyService(familyDao);
    }

    @Test
    public void testCreateNewFamily() {
        Human mother = new Human("Mother");
        Human father = new Human("Father");
        when(familyDao.createNewFamily(mother, father)).thenReturn(true);
        boolean result = familyService.createNewFamily(mother, father);
        assertTrue(result);
    }

    @Test
    public void testDeleteFamilyByIndex() {
        when(familyDao.deleteFamily(0)).thenReturn(true);
        boolean result = familyService.deleteFamilyByIndex(0);
        assertTrue(result);
    }

    @Test
    public void testAdoptChild() {
        Family family = new Family(new Human("Mother"), new Human("Father"));
        Human child = new Human("Child");
        when(familyDao.saveFamily(family)).thenReturn(true);
        Family result = familyService.adoptChild(family, child);
        assertEquals(family, result);
        assertTrue(family.getChildren().contains(child));
    }


    @Test
    public void testGetFamilyById() {
        Family family = new Family(new Human("Mother"), new Human("Father"));
        when(familyDao.getFamilyById(0)).thenReturn(family);
        Family result = familyService.getFamilyById(0);
        assertEquals(family, result);
    }


    @Test
    public void testDeleteAllChildrenOlderThan() {
        doNothing().when(familyDao).deleteAllChildrenOlderThen(18);
        familyService.deleteAllChildrenOlderThan(18);
        verify(familyDao, times(1)).deleteAllChildrenOlderThen(18);
    }
}
