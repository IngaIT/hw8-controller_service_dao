
package com.family;

import com.family.controller.FamilyController;
import com.family.dao.CollectionFamilyDao;
import com.family.dao.FamilyDao;
import com.family.model.Dog;
import com.family.model.DomesticCat;
import com.family.model.Human;
import com.family.model.Pet;
import com.family.service.FamilyService;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);

        FamilyController familyController = new FamilyController(familyService);

        Set<String> dogHabits = new HashSet<>(Arrays.asList("їсть", "грається", "спить"));
        Dog dog = new Dog("Барбос", "Buddy", 75, dogHabits);

        Set<String> catHabits = new HashSet<>();
        DomesticCat cat = new DomesticCat( Pet.Species.CAT, "Мурзік", 2, 40, catHabits);

        Map<Human.DayOfWeek, Human.Activity> father1Schedule = new HashMap<Human.DayOfWeek, Human.Activity>();
        father1Schedule.put(Human.DayOfWeek.MONDAY, Human.Activity.WORK);
        father1Schedule.put(Human.DayOfWeek.TUESDAY, Human.Activity.WORK);
        father1Schedule.put(Human.DayOfWeek.FRIDAY, Human.Activity.THEATRE);
        father1Schedule.put(Human.DayOfWeek.SATURDAY, Human.Activity.SWIMMING);
        Human father1 = new Human("Олег", "Іванов", 1975, 130, father1Schedule);

        Human mother1 = new Human("Олена", "Іванова", 1977);
        Human child1 = new Human("Марія", "Іванова", 2001);

        Human father2 = new Human("Андрій", "Петров", 1980);
        Human mother2 = new Human("Наталія", "Петрова", 1982);

        Map<Human.DayOfWeek, Human.Activity> child2Schedule = new HashMap<>();
        child2Schedule.put(Human.DayOfWeek.MONDAY, Human.Activity.WORK);
        child2Schedule.put(Human.DayOfWeek.WEDNESDAY, Human.Activity.STUDY);
        child2Schedule.put(Human.DayOfWeek.THURSDAY, Human.Activity.CINEMA);
        child2Schedule.put(Human.DayOfWeek.SUNDAY, Human.Activity.SLEEP);
        Human child2 = new Human("Максим", "Петров", 2005, 100, child2Schedule);

        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother2, father2);
        familyController.adoptChild(familyController.getFamilyById(0), child1);
        familyController.adoptChild(familyController.getFamilyById(1), child2);
        familyController.addPets(0, dog);
        familyController.addPets(1, cat);

        familyController.displayAllFamilies();

        familyController.bornChild(familyController.getFamilyById(0), "Марк", "Анна");
        familyController.bornChild(familyController.getFamilyById(1), "Іван", "Наталя");

        familyController.displayAllFamilies();

        System.out.println("Number of families with 4 members: " + familyController.countFamiliesWithMemberNumber(4));

        familyController.deleteFamilyByIndex(1);

        familyController.displayAllFamilies();
    }
}
