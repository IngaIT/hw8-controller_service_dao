package com.family.dao;
import com.family.model.Family;
import com.family.model.Human;
import com.family.model.Pet;
import com.family.service.FamilyService;

import java.util.List;

public interface FamilyDao {
        List<Family> getAllFamilies();
        Family getFamilyByIndex(int index);
        boolean deleteFamily(int index);
        boolean saveFamily(Family family);
        void displayAllFamilies();
        List<Family> getFamiliesBiggerThan(int count);
        List<Family> getFamiliesLessThan(int count);
        int countFamiliesWithMemberNumber(int count);
        boolean createNewFamily(Human father, Human mother);
        Family bornChild(Family family, String manName, String womanName);
        Family adoptChild(Family family, Human child);
        void deleteAllChildrenOlderThen(int age);
        int count();
        Family getFamilyById(int id);
        List<Pet> getPets(int familyIndex);
        boolean addPets(int familyIndex, Pet pet);
    }




