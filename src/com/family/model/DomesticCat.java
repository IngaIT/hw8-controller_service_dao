package com.family.model;

import java.util.Set;

public class DomesticCat extends Pet implements Foulable {
    public DomesticCat(String nickname) {
        super(nickname);
        setSpecies(Pet.Species.CAT);
    }

    public DomesticCat(Pet.Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        super(species, nickname, age, trickLevel, habits);
    }

    public DomesticCat() {
    }

    @Override
    public void respond() {
        System.out.println("Привіт, я кіт " + this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println("Я кіт і я роблю гидоти...");
    }
}